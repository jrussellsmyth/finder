#ifdef __EMX__
# define COMPILER "EMX/GCC"
# include <io.h>
#endif /*__EMX__*/
#ifdef __MINGW32__
# define COMPILER "Minamalist GNU-WIN32"
#endif /*__MINGW32__*/
#ifdef __CYGWIN32__
# define COMPILER "Cygnus GNU-WIN32"
#endif
#ifdef __LCC__
# define COMPILER "LCC"
#endif
#ifdef __DJGPP__
# define COMPILER "DJGPP"
#endif
#ifdef __osf__
# define COMPILER "DEC OSF/1 CC"
#endif
#ifdef __linux__
#endif
#ifdef __GNUC__
# define COMPILER "GCC"
#endif
#ifndef COMPILER
# define COMPILER "unknown"
#endif
#ifndef FINDER_OS
# define FINDER_OS "unknown"
#endif
#ifndef MAX_PATH 
#define MAX_PATH     (260)
#endif

#define ENVIRON_OPT "FINDER_OPT"
char *add_envopt(int *argcp, char ***argvp, char *env);
void * xmalloc (unsigned size);
void mainsearch(char * file,char * search);
int useage();

