/***************************************************************************
*   FINDER - text file search utility                                      *
*   FileName FINDER.C                                                      *
*                                                                          *
*   Written By    - J.Russell Smyth                                        *
*   Date Written  - 01/23/96                                               *
*   Date Modified - 02/04/96 - added recurse and case insensitive options  *
*   Date Modified - 07/27/96 - changed search routine to b-m-h algorithm   *
*   Date Modified - 10/23/96 - added list file output option to go with    *
*                              listfile input option avail through DJGPP   *
*   Ver 2.6b  -jrs- 01/27/97 - changed to set return value based on find/  *
*                              no find, or bad args                        *
*                              0=found, 1=not found, 2=bad/no args         *
*   Ver 3.0.0 -jrs- 10/11/97 - almost total rewrite to make more unix like *
*                              and functional	                           *
*   Ver 3.1.0 -jrs- 11/02/97 - release BETA of new version. Final feature  * 
*                              release. Stable version will be 3.2.0. no   *
*                              new features or devel will be added         *
*   Ver 3.1.1 -jrs- 11/04/97 - BUGFIX - fixed always recurse bug           *
*   Ver 3.1.2 -jrs- 12/12/97 - made minor changes for OSF/1                *
*                              made OSF/1 makefile                         *
*   VER 3.1.3 -jrs- 11/09/98 - Changed to autoconf build                   *
*                              Fixed seg-fault when using -a and -i        *
*   VER 3.1.4 -jrs- 07/12/99 - BUGFIX when using -u and -a would give line *
*                              numbers on every line after the first found *
*                                                                          *
/**************************************************************************/
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include "getopt.h"
#include "finder.h"
#include <tsrch.h>
#ifdef __LCC__
#define S_ISDIR(m)      (((m) & S_IFMT) == S_IFDIR)
#endif

 /* option flags */
char * progname;
int recursive;      /* recurse directories                -r */
int all_occurances; /* list all occurances                -a */
int no_heading;     /* no file name headings              -n */
int filelist_only;  /* list files containing match only   -l */
int files_first;    /* file-names first for compatability -o */
int use_stdin=0;    /* search stdin (calculated option)      */
int label;			/* label each line with filename	  -b */
int no_label;       /* supress all line labeling          -u */
int input_list;		/* use file name for input file list  -f <arg> */
char * input_file;   /* pointer to input file name			 */
unsigned long sflags;/* storage for search flags*/




/* options structures for long options*/
struct option longopts[] =
{
 /* { name  has_arg  *flag  val } */
    {"recursive",     0, 0, 'r'},
    {"all-occurances",0, 0, 'a'},
    {"no-case-sense",0,0,'i'},
    {"no-heading",0,0,'n'},
    {"regex",0,0,'x'},
    {"regex-extended",0,0,'X'},
    {"file-list-only",0,0,'l'},
    {"old-arguments",0,0,'o'},
    {"help",0,0,'h'},
    {"help-regex",0,0,'e'},
    {"version",0,0,'v'},
    {"copyright",0,0,'C'},
    {"label-lines",0,0,'b'},
    {"un-labeled",0,0,'u'},
    { 0, 0, 0, 0 }
};


int final_return=1;   /* to track final program return value */


/*Main*/
int main(int argc, char *argv[])
{
int optc,srchndx,x; /*current option*/
char * env=NULL;
char sfilename[256];
struct stat s;
HTSRCH lpTsrch;
TS_FIND_DATA found;
FILE * sfile;
FILE * list;
    progname=argv[0]; 
    if (!argv[1]) useage();
    env=add_envopt(&argc,&argv,ENVIRON_OPT);
    while ((optc = getopt_long (argc, argv, "abCef:hilnoruvxX",
				longopts, (int *)0)) != EOF)
    {
		switch (optc)
		{
			case 'r' :  recursive=(recursive==1?0:1);
				break;
	
			case 'a' :  all_occurances=(all_occurances==1?0:1);
				break;
			case 'b' :  label=(label==1?0:1);
				break;
			case 'u' :  no_label=(no_label==1?0:1);
				break;
			case 'f' :	input_list=(input_list==1?0:1);input_file=optarg;
				break;
				
			case 'i' :  sflags=sflags^TS_IGNORECASE;
				break;
			case 'x' :  sflags=sflags^TS_REGEX;
				break;
			case 'X' :  sflags=sflags^TS_REGEXEX;
				break;
			case 'l' :  filelist_only=(filelist_only==1?0:1);
				break;
			case 'n' :  no_heading=(no_heading==1?0:1);
				break;
			case 'o' :  files_first=(files_first==1?0:1);
				break;
			case 'v' :  version();
			case 'e' :  regexhelp();
			case 'C' :  copyrights();
			case 'h' :
			default  :  useage();
		}
    }
    if (!argv[optind]) useage();/* must have at one non-option arg*/
	if ((!argv[optind+1])&&(!input_list)) {
		use_stdin=1;
		srchndx=--argc;
	}	
	else{
	    if (files_first) {
			srchndx=--argc;
	    }else{
			srchndx=optind++;
	    }
	}   
    if (use_stdin){
		if((lpTsrch=tsFindFirst(stdin,argv[srchndx],sflags,&found))!=NULL){
				final_return=0;
	      		if(no_label)
	      			printf("\n%s",found.linecontents);
	      		else
	      			printf("\n%d:%s",found.linenum,found.linecontents);
	      		if(all_occurances){
		        		while(!tsFindNext(lpTsrch,&found))
							if(no_label)
		        				printf("%s",found.linecontents);
		        			else
		        				printf("%d=>%s",found.linenum,
		        						found.linecontents);
				}
		}
    }
    else{
  		fprintf(stderr,"searching for %s\n",argv[srchndx]);
		if (input_list){
			if((list=fopen(input_file,"rt"))==NULL){
					fprintf(stderr,"\ninput file %s not found",input_file);
					useage();
			}
			while (fgets(sfilename,255,list)!=NULL){
				if(sfilename[strlen(sfilename)-1]=='\n'){
					sfilename[strlen(sfilename)-1]='\0';
				}
	    		mainsearch(sfilename,argv[srchndx]);
			}
		}else{
	    	while (optind < argc) {
				stat(argv[optind],&s);
				if (S_ISDIR(s.st_mode)){
					if (recursive)                           /* ver 3.1.1 */
			    		dodir(argv[optind],argv[srchndx]);
			    	else                                     /* ver 3.1.1 */
			    		fprintf(stderr,"skipping directory %s\n",argv[optind]); /* ver 3.1.1 */
				}
		    	else{
		    		mainsearch(argv[optind],argv[srchndx]);
				}
			optind++;
			}
		}
	}
	return final_return;
}
int dodir(char * directory,char * searchstring)
{
	char newfile[MAX_PATH];
	DIR * df;
	static struct dirent * ds;
	struct stat s2;
	 df=opendir (directory);
 	while ((ds=readdir(df))!=NULL){
 		if((!strcmp(".",ds->d_name))||(!strcmp("..",ds->d_name)))
 		;
 		else{
			strcpy(newfile,directory);
#ifdef _WIN32
 			strcat(newfile,"\\");
#else
                        strcat(newfile,"/");
#endif
 			strcat(newfile,ds->d_name);
  			stat(newfile,&s2);
			if (S_ISDIR(s2.st_mode))
	   			dodir(newfile,searchstring);
			else
				mainsearch(newfile,searchstring);
		}
	}

	  	
  if(closedir(df)){
  	fprintf(stderr,"error closing directory!");
  	exit(2);
  }
  return 0;
}

void mainsearch(char * fname,char * sstring)
{
HTSRCH lpTsrch;
TS_FIND_DATA found;
FILE * sfile;
	if ((sfile=fopen(fname,"rb"))==NULL){
		fprintf(stderr,"\n file %s not found",fname);
   	}
   	else{
       	if((lpTsrch=tsFindFirst(sfile,sstring,sflags,&found))!=NULL){
       		final_return=0;
       		if(!filelist_only){
				if(!no_heading){
					fprintf(stdout,"\nfound match in %s\n",fname);
				}
				if(label)
					printf("%s:%d:%s",fname,found.linenum,found.linecontents);
				else if(no_label)
	   				printf("%s",found.linecontents);
				else
	   				printf("%d:%s",found.linenum,found.linecontents);
	       		if(all_occurances){
	        		while(!tsFindNext(lpTsrch,&found))
						if(label)
	        				printf("%s:%d:%s",fname,found.linenum,
		       					found.linecontents);
						else if(no_label)
	   						printf("%s",found.linecontents);
		       			else
		       				printf("%d:%s",found.linenum,
		       					found.linecontents);
		        }
		    }
		    else{
						fprintf(stdout,"\n %s",fname);
			}
		}
				fclose(sfile);
				tsFindClose(lpTsrch);
	}
}

int useage()
{
	printf("\nFinder version %s",VERSION);
	printf(" Copyright (C) 1997 J Russell Smyth. Distribute freely.\n");
	printf(" NO WARRANTEE. This program is protected by the");
	printf(" GNU General Public License.\n\n");
	printf("Usage: finder -aehilnovxX <search-string> [-f <filename>|"
			"<filespec>|]\n");
	printf("  -a --all-occurances     report all occurances in file\n");
	printf("  -b --label-lines        label each found line with filename\n");
	printf("  -u --un-labeled         supress all line labeling\n");
	printf("  -r --recursive          recursively search directories\n");
	printf("  -i --no-case-sense      case insensitive search\n");
	printf("  -x --regex              search-string is regular expression\n");
	printf("  -X --regex-extended     search-string is extended regular expression\n");
	printf("  -l --file-list-only     list file names only\n");
	printf("  -n --no-heading         no filename header\n");
	printf("  -o --old-arguments      reverse search-string and filespec"
			" on command line\n");
	printf("  -f <filename>           use <filename> as list of files to"
			" search\n");
	printf("  -h --help               print help and exit\n");
	printf("  -e --help-regex         print regular expression summary and "
			"exit\n");
	printf("  -v --version            print version and compile date/time"
			" and exit\n");
	printf("  -C --copyright          print copyright info and exit\n\n");
	printf("Returns: 0=found match, 1=no match found, 2=bad argument/help "
		"screen call\n");
	printf("  if no files given to search, stdin will be searched\n");
	printf("  options may be contained in the environment in %s\n",
			ENVIRON_OPT);
	printf("  any option in %s may be overridden by repeating on command"
			" line\n",ENVIRON_OPT);
	exit(2);
}
/* ======================================================================== */
int version()
{
    fprintf(stderr,"%s %s for %s\n  Built %s-%s with %s\n",
	   progname,VERSION,FINDER_OS, __DATE__,__TIME__,COMPILER);
    exit(2);
}
int regexhelp()
{
	printf("\n Regular Expression Summary");
	printf("\n ==========================");
	printf("\n \"abcd\" -- matching a string literally     \"^\" -- negate");
	printf("\n \".\" -- matching everything except NULL");
	printf("\n \"[a-z_?]\",\"^[a-z_?]\",\"[[:alpha:]]\" and \"[^[:alpha:]]\" -- matching character");
	printf("\n                                                          sets");
	printf("\n \"\\(subexp\\)\" -- grouping an expression into a subexpression.");
	printf("\n \"\\n\" -- match a copy of whatever was matched by the nth subexpression.");
	printf("\n");
	printf("\nThe following special characters and sequences can be applied to a");
	printf("\ncharacter, character set, subexpression, or backreference:");
	printf("\n");
	printf("\n \"*\" -- repeat the preceeding element 0 or more times.");
	printf("\n \"\\+\" -- repeat the preceeding element 1 or more times.");
	printf("\n \"\\?\" -- match the preceeding element 0 or 1 time.");
	printf("\n \"{m,n}\" -- match the preceeding element at least  \"m\", and as");
	printf("\n              many as  \"n\" times.");
	printf("\n \"regexp-1\\|regexp-2\\|..\" -- match any regexp-n.");
	printf("\n");
	printf("\nA special character, like  \".\" or  \"*\" can be made into a literal");
	printf("\ncharacter by prefixing it with  \"\\\".");
	printf("\n");
	printf("\nA special sequence, like  \"\\+\" or  \"\\?\" can be made into a");
	printf("\nliteral character by dropping the  \"\\\".");
	
	exit(2);
}
int copyrights()	
{
	printf(" FINDER  Copyright (C) 1997 J Russell Smyth.\n"
			"   Distributed under the GNU GENERAL PUBLIC LICENSE\n");
	printf(" LIBTSRCH  Copyright (C) 1997 J Russell Smyth.\n"
			"   Distributed under the GNU GENERAL PUBLIC LICENSE\n");
	printf(" Boyer-Moore-Horspool pattern match public domain by Raymond Gardner 7/92\n");
	printf(" Case-insensitive Boyer-Moore-Horspool pattern match public Domain version \n"
			"  by Thad Smith 7/21/1992, based on a 7/92 public domain BMH version by\n"
			"  Raymond Gardner\n");
	printf(" RX by Tom Lord, protected by the GNU LIBRARY GENERAL PUBLIC LICENSE \n");
	printf("\n Copies of the GNU GENERAL PUBLIC LICENSE and the GNU LIBRARY GENERAL PUBLIC \n"
			"  LICENSE should have accompanied this software\n");
	exit(2);
}
	
