/* add_envopt and xmalloc
 * borrowed from util.c/util.h in the gnu gzip package, modified to stand
 * alone
 * ========================================================================*/
#include <ctype.h>
#include <stdio.h>
#define __need_NULL
#include <stddef.h>
void * xmalloc (unsigned size);
/* ========================================================================
 * Add an environment variable (if any) before argv, and update argc.
 * Return the expanded environment variable to be freed later, or NULL
 * if no options were added to argv.
 */
#define SEPARATOR	" \t"	/* separators in env variable */

char *add_envopt(argcp, argvp, env)
    int *argcp;          /* pointer to argc */
    char ***argvp;       /* pointer to argv */
    char *env;           /* name of environment variable */
{
    char *p;             /* running pointer through env variable */
    char **oargv;        /* runs through old argv array */
    char **nargv;        /* runs through new argv array */
    int	 oargc = *argcp; /* old argc */
    int  nargc = 0;      /* number of arguments in env variable */

    env = (char*)getenv(env);
    if (env == NULL) return NULL;

    p = (char*)xmalloc((unsigned)strlen(env)+1);
    env = (char*)strcpy(p, env);                    /* keep env variable intact */

    for (p = env; *p; nargc++ ) {            /* move through env */
	p += strspn(p, SEPARATOR);	     /* skip leading separators */
	if (*p == '\0') break;

	p += strcspn(p, SEPARATOR);	     /* find end of word */
	if (*p) *p++ = '\0';		     /* mark it */
    }
    if (nargc == 0) {
	free(env);
	return NULL;
    }
    *argcp += nargc;
    /* Allocate the new argv array, with an extra element just in case
     * the original arg list did not end with a NULL.
     */
    nargv = (char**)calloc(*argcp+1, sizeof(char *));
    if (nargv == NULL) {
    	fprintf(stderr,"error:add_envopt:out of memory");
         exit(3);
    }
    oargv  = *argvp;
    *argvp = nargv;

    /* Copy the program name first */
    if (oargc-- < 0) {
    	fprintf(stderr,"error:add_envopt:argc<=0");
         exit(3);
    }
    *(nargv++) = *(oargv++);

    /* Then copy the environment args */
    for (p = env; nargc > 0; nargc--) {
	p += strspn(p, SEPARATOR);	     /* skip separators */
	*(nargv++) = p;			     /* store start */
	while (*p++) ;			     /* skip over word */
    }

    /* Finally copy the old args and add a NULL (usual convention) */
    while (oargc--) *(nargv++) = *(oargv++);
    *nargv = NULL;
    return env;
}

/* ========================================================================
 * Semi-safe malloc -- never returns NULL.
 */
void * xmalloc (size)
    unsigned size;
{
    void * cp = (void *)malloc (size);

    if (cp == NULL) {
    	fprintf(stderr,"error:add_envopt:out of memory");
         exit(3);
    }
    return cp;
}



