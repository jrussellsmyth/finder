#include <stdio.h>
#include "tsrch.h"
#include "search.h"
/**************************************************************************
* tsFindInit - initialize search structure
***************************************************************************/
HTSRCH tsFindInit(char * searchstring,unsigned long searchflags,
				LPTS_FIND_DATA lpTSFindData);
/**************************************************************************
* tsFindFirst - initialize search structure and find first matching string 
*               in file
***************************************************************************/
HTSRCH tsFindFirst(FILE * file,char * searchstring,
				unsigned long searchflags,LPTS_FIND_DATA lpTSFindData)
{
	int linecount=0;
	char * ret=NULL;
	char line[MAX_TS_RETURN_STRING];
	int regexflags=0;
	int rxret=1;
	char buff[50];
	HTSRCH lpHtsrch;
	regex_t tmprxcomp;
	if ((searchflags&TS_REGEX)||(searchflags&TS_REGEXEX)){
		if(searchflags&TS_IGNORECASE)
			regexflags|=REG_ICASE;
		if(searchflags&TS_REGEXEX)
			regexflags|=REG_EXTENDED;
		regcomp (&tmprxcomp, searchstring, regexflags);
	}
	else{
		if (searchflags&TS_IGNORECASE) 
			bmhi_init(searchstring);
		else 
			bmh_init(searchstring);
	}
	while (fgets(line,255,file)!=NULL)
	{
                ++linecount;
		if ((searchflags&TS_REGEX)||(searchflags&TS_REGEXEX)){
			rxret=regexec(&tmprxcomp,line,0,NULL,0);
			regerror (rxret,&tmprxcomp, buff, 50);
		}
		else{
			if(searchflags&TS_IGNORECASE)
				ret=bmhi_search(line,strlen(line));
			else
				ret=bmh_search(line,strlen(line));
		}
		if ((rxret==0)||(ret!=NULL)){
			lpHtsrch = (HTSRCH) malloc (sizeof (TSRCH));
			lpHtsrch->file=file;
			lpHtsrch->flags=searchflags;
			lpHtsrch->hitpos=linecount;
			lpHtsrch->rxcomp=tmprxcomp;
			strcpy(lpTSFindData->linecontents,line);
			lpTSFindData->linenum=linecount;
			return lpHtsrch;
		}
	}
	return NULL;
}
/**************************************************************************
* tsFindNext - find next match in file from 
**************************************************************************/
int tsFindNext(HTSRCH lpHtsrch,LPTS_FIND_DATA lpTSFindData)
{
	int linecount;
	char * ret=NULL;
	char line[MAX_TS_RETURN_STRING];
	char buff[50];
	int rxret=1;
	regex_t tmprxcomp;
	tmprxcomp=lpHtsrch->rxcomp;
	linecount=lpHtsrch->hitpos;
	while (fgets(line,255,lpHtsrch->file)!=NULL)
	{
		++linecount;
		if ((lpHtsrch->flags&TS_REGEX)||(lpHtsrch->flags&TS_REGEXEX)){
			rxret=regexec(&tmprxcomp,line,0,NULL,0);
			regerror (rxret,&tmprxcomp, buff, 50);
		}
		else{
			if(lpHtsrch->flags&TS_IGNORECASE)
				ret=bmhi_search(line,strlen(line));
			else
				ret=bmh_search(line,strlen(line));
		}
		if ((ret!=NULL)||(rxret==0)){
			strcpy(lpTSFindData->linecontents,line);
			lpTSFindData->linenum=linecount;
			lpHtsrch->hitpos=linecount;
			return 0;
		}
	}
	if(lpHtsrch->flags&TS_IGNORECASE)
		bhmi_cleanup();
	return 1;
}
/**************************************************************************
* tsFindPrev - find match searching backwards
* ************************************************************************/
int tsFindPrev(HTSRCH lpHtsrch,LPTS_FIND_DATA lpTSFindData)
{
	return 1;
}
/**************************************************************************
* tsFindLast - find match nearest end of file
* ************************************************************************/
int tsFindLast(HTSRCH lpHtsrch,LPTS_FIND_DATA lpTSFindData)
{
	return 1;
}
/**************************************************************************
* tsFindClose - test for search match within string
* ************************************************************************/
int tsFindInString(HTSRCH lpHtsrch,char * string,LPTS_FIND_DATA lpTSFindData)
{
	char * ret=NULL;
	char line[MAX_TS_RETURN_STRING];
	char buff[50];
	int rxret=1;
	regex_t tmprxcomp;
	tmprxcomp=lpHtsrch->rxcomp;
	if ((lpHtsrch->flags&TS_REGEX)||(lpHtsrch->flags&TS_REGEXEX)){
		rxret=regexec(&tmprxcomp,string,0,NULL,0);
		regerror (rxret,&tmprxcomp, buff, 50);
	}
	else{
		if(lpHtsrch->flags&TS_IGNORECASE)
			ret=bmhi_search(string,strlen(string));
		else
			ret=bmh_search(string,strlen(string));
	}
	if ((ret!=NULL)||(rxret==0)){
		return 0;
	}
	return 1;
}
/**************************************************************************
* tsFindClose - release search handle
* ************************************************************************/
int tsFindClose(HTSRCH lpHtsrch);int tsFindClose(HTSRCH lpHtsrch)
{
	free(lpHtsrch);
	return 0;
}
