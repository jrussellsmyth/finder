/* tsrch.h - Header for J.Russell's Text Search Library */
/*	Copyright (C)  1997 J.Russell Smyth
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#include <regex.h>

#define TS_IGNORECASE    (1)
#define TS_REGEX         (2)
#define TS_REGEXEX       (4)
#define TS_USEPBM        (8)
#define TS_RESERVED01    (16)
#define TS_RESERVED02    (32)
#define TS_RESERVED03	 (64)
#define TS_RESERVED04	 (128)
#define TS_RESERVED05    (256)
#define TS_RESERVED06    (512)
#define TS_RESERVED07	 (1024)
#define TS_RESERVED08    (2048)
#define TS_RESERVED09    (4096)
#define TS_RESERVED010   (8192)
#define TS_RESERVED011   (16384)

#ifndef MAX_TS_RETURN_STRING
# define MAX_TS_RETURN_STRING (256)
#endif
typedef struct _TSSRCHSTRUCT{
	FILE * file;
	unsigned long  flags;
	unsigned long  hitpos;
    regex_t rxcomp;
    }TSRCH;
typedef TSRCH * HTSRCH;
typedef struct _TS_FIND_DATA{
	unsigned long linenum;
	unsigned long charpos;
	long reserved01;
	long reserved02;
	char linecontents[MAX_TS_RETURN_STRING];
	}TS_FIND_DATA;
typedef TS_FIND_DATA * LPTS_FIND_DATA;	

HTSRCH tsInit(char * searchstring,unsigned long searchflags,
				LPTS_FIND_DATA lpTSFindData);
HTSRCH tsFindFirst(FILE * file,char * searchstring,unsigned long searchflags,
				LPTS_FIND_DATA lpTSFindData);
int tsFindNext(HTSRCH htsrch,LPTS_FIND_DATA lpTSFindData);
int tsFindPrev(HTSRCH htsrch,LPTS_FIND_DATA lpTSFindData);
int tsFindLast(HTSRCH htsrch,LPTS_FIND_DATA lpTSFindData);
int tsFindInString(HTSRCH htsrch,char * string,LPTS_FIND_DATA lpTSFindData);
int tsFindClose(HTSRCH htsrch);
